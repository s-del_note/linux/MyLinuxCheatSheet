# systemctl
[systemd](https://ja.wikipedia.org/wiki/Systemd)


## 起動・停止・再起動・再読み込み
| コマンド | 説明・補足 |
| --- | --- |
| `# systemctl start <ユニット名>` | 起動 |
| `# systemctl stop <ユニット名>` | 停止 |
| `# systemctl restart <ユニット名>` | 再起動 |
| `# systemctl reload <ユニット名>` | 再読み込み (Unit ファイルで reload が定義されていれば利用できる) |


## サービスの状態確認
- `$ systemctl status <ユニット名>`
- `$ journalctl -u <ユニット名>`
- `$ journalctl -f` : `tail -F` の様にログを監視
- `$ journalctl -n <行数>` : ログの末尾のみ表示


## サービス自動起動
| コマンド | 説明・補足 |
| --- | --- |
| `# systemctl enable <ユニット名>` | 自動起動有効化 |
| `# systemctl disable <ユニット名>` | 自動起動無効化 |
| `# systemctl is-enable <ユニット名>` | 自動起動の状態を確認 |


## 設定ファイルの配置
`/etc/systemd/system` にユニットファイル (.service) やタイマーファイル (.timer) 等を配置することで、  
自作のプログラムやスクリプト等の自動起動や実行のスケジュールを行うことができる。  
かなり細かく設定を行えるのでここでの記載は割愛。参考記事を参照。


## 設定ファイルの再読み込み
`# systemctl daemon-reload` 設定ファイル変更時に systemd に認識させる


## 参考
- [systemd - ArchWiki](https://wiki.archlinux.jp/index.php/Systemd)
- [systemd のユニットファイルの作り方 | 晴耕雨読](https://tex2e.github.io/blog/linux/create-my-systemd-service)