# Linux Cheat Sheet
この辺を押さえておけば最低限の Linux の操作が出来るであろうというコマンドの備忘録  
個人的な備忘録のため、特殊な使い方や踏み込んだ使い方であったり、全てのオプションを網羅しているわけではない。

## 目次
<ul>
    <li><a href="./基本操作.md">基本操作</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>カレントディレクトリの表示</li>
                <li>カレントディレクトリの変更 (移動)</li>
                <li>コンソール操作</li>
                <li>パイプ</li>
                <li>リダイレクト</li>
                <li>スーパーユーザー</li>
            </ul>
        </details>
    </li>
    <li><a href="./ファイル・ディレクトリ操作.md">ファイル・ディレクトリ操作</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>コピー</li>
                <li>移動 (切り取り)</li>
                <li>名前変更</li>
                <li>削除</li>
                <li>閲覧</li>
                <li>検索</li>
                <li>リンク</li>
                <li>所有者変更</li>
                <li>権限変更</li>
            </ul>
        </details>
    </li>
    <li><a href="./システム・ハードウェア情報.md">システム・ハードウェア情報</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>CPU</li>
                <li>RAM</li>
                <li>Disk</li>
                <li>kanel</li>
                <li>稼働時間とCPU使用率</li>
                <li>ログイン中のユーザー一覧</li>
                <li>実行中のプロセス一覧</li>
                <li>プロセスを終了</li>
                <li>ロケール情報</li>
            </ul>
        </details>
    </li>
    <li><a href="./ユーザー操作.md">ユーザー操作</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>ユーザー一覧</li>
                <li>ユーザー作成</li>
                <li>ユーザー削除</li>
                <li>パスワード</li>
                <li>ユーザー設定変更</li>
                <li>所属グループの一覧表示</li>
            </ul>
        </details>
    </li>
    <li><a href="./グループ操作.md">グループ操作</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>グループ一覧</li>
                <li>グループ作成</li>
                <li>グループ設定変更</li>
                <li>グループにユーザーを加える</li>
                <li>グループからユーザーを削除する</li>
            </ul>
        </details>
    </li>
    <li><a href="./環境変数.md">環境変数</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>ユーザー環境変数</li>
                <li>システム環境変数</li>
            </ul>
        </details>
    </li>
    <li><a href="./alias.md">alias</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>一時的な変更</li>
                <li>永続的な変更</li>
            </ul>
        </details>
    </li>
    <li><a href="./apt.md">apt</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>更新</li>
                <li>インストール・アンインストール</li>
                <li>詳細表示・検索</li>
                <li>インストール済みパッケージの一覧の表示・ログの表示</li>
                <li>deb</li> ファイル削除
            </ul>
        </details>
    </li>
    <li><a href="./systemctl.md">systemctl</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>起動・停止・再起動・再読み込み</li>
                <li>サービスの状態確認</li>
                <li>サービス自動起動</li>
                <li>設定ファイルの配置</li>
                <li>設定ファイルの再読み込み</li>
            </ul>
        </details>
    </li>
    <li><a href="./ufw.md">ufw</a>
        <details>
            <summary>内容</summary>
            <ul>
                <li>ufw</li> 自体の有効化・無効化・再起動
                <li>現在のファイアウォールルール一覧表示</li>
                <li>ルール追加</li>
                <li>ルール削除</li>
            </ul>
        </details>
    </li>
</ul>