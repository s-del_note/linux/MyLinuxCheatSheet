# apt
[APT](https://ja.wikipedia.org/wiki/APT) は Debian 系 Linux ディストリビューションで採用されるコマンドラインベースの[パッケージ管理システム](https://ja.wikipedia.org/wiki/%E3%83%91%E3%83%83%E3%82%B1%E3%83%BC%E3%82%B8%E7%AE%A1%E7%90%86%E3%82%B7%E3%82%B9%E3%83%86%E3%83%A0)  
Advanced Packaging Tool または Advanced Package Tool の略  


## 更新
| コマンド | 説明・補足 |
| --- | --- |
| `# apt update` | パッケージリストを更新[^1] |
| `# apt upgrade` | インストール済みパッケージを更新 |
| `# apt full-upgrade` | 更新が保留されているパッケージを更新 |


## インストール・アンインストール
| コマンド | 説明・補足 |
| --- | --- |
| `# apt install <パッケージ名 | deb ファイルのパス>` | インストール |
| `# apt remove <パッケージ名>` | 削除 |
| `# apt remove --purge <パッケージ名>` | 完全削除 |
| `# apt autoremove` | 更新に伴い不要になったパッケージを削除 (apt 実行時にこのコマンドを実行するように指示があれば実行) |


## 詳細表示・検索
| コマンド | 説明・補足 |
| --- | --- |
| `$ apt show <パッケージ名>` | 詳細表示 |
| `$ apt list <パッケージ名>` | 完全一致検索 |
| `$ apt search <パッケージ名>` | 部分一致検索 |


## インストール済みパッケージ一覧の表示・ログの表示
| コマンド | 説明・補足 |
| --- | --- |
| `$ apt list --installed` | インストール済みパッケージ一覧を表示 |
| `$ apt list --installed | grep <パッケージ名>` | 該当するインストール済みパッケージを表示 |
| `$ dpkg -l` | インストール済みパッケージ一覧を表示 (dpkg) |
| `$ less /var/log/apt/history.log` | apt コマンドのログを表示 (less) |


## deb ファイル削除
| コマンド | 対象 |
| --- | --- |
| `# apt autoclean` | キャッシュ済み未インストールの deb ファイル|
| `# apt clean` | キャッシュ済み全ての deb ファイル|


## 参考
- [aptコマンドチートシート - Qiita](https://qiita.com/SUZUKI_Masaya/items/1fd9489e631c78e5b007)
- [apt-get updateは何を行っているのか調べてみた〜パッケージインデックスファイルとは何か〜 - 君は心理学者なのか？](https://karoten512.hatenablog.com/entry/2018/01/09/003330)
- [Ubuntu パッケージ管理 その2 - リポジトリの基本 - kledgeb](https://kledgeb.blogspot.com/2012/07/ubuntu-2.html)
- [APT の設定 (/etc/apt/sources.list) をちゃんと理解する - くじらにっき++](https://kujira16.hateblo.jp/entry/2019/10/14/190008)


[^1]: インストール可能なパッケージの一覧を更新する。`/etc/apt/source.list` に記載されている debian リポジトリ URL に問い合わせ、パッケージインデックスファイルを `/var/lib/apt/lists` に格納する。